library(Seurat)
library(Signac)
library(SeuratData)
library(plyr)
library(dplyr)
library(harmony)
library(SingleCellExperiment)
library(reshape2)
library(data.table)
library(tibble)
library(Matrix.utils)
library(SeuratWrappers)
library(ChIPseeker)
library(TxDb.Hsapiens.UCSC.hg38.knownGene)
library(clusterProfiler)
library(org.Hs.eg.db)
library(purrr)
library(tidyr)
library(magrittr)
library(UpSetR)

homer_path_bin <- "homertools/bin/"


load(file = "data.RData")
aggr3.anchors.combined = obj_aggr4


### ATAC annotated peaks PSEUDOBULK

txdb = TxDb.Hsapiens.UCSC.hg38.knownGene
peaks_to_genes = annotatePeak(peak = granges(aggr3.anchors.combined@assays$peaks), TxDb = txdb, annoDb="org.Hs.eg.db")

# Differentially accessible regions ###

aaa1 <- as.data.frame(peaks_to_genes@anno)
aaa1$Strand <- ifelse(aaa1$geneStrand == 1, "+", "-")
aaa1$uid <- paste0("peak", 1:nrow(aaa1))



########################################  By cell type ########################################  

cells <- c("B_cells", "CD4_mature_T_cells", "CD4_naive_T_cells", "CD8_T_cells", "CD14_monocytes", "CD16_monocytes", "Dendritic_cells", "Inflammatory_T_cells", "NK_cells", "Pseudobulk")

for(c in cells) {
  # Differentially accessible regions
  dat <- openxlsx::read.xlsx("Genes_Univariate_forPathway.xlsx")
  mapping <- readRDS(paste( c, "_mapping.rds", sep=""))
  mapping$uid2 <- paste(mapping$chr, mapping$start, mapping$stop, sep="_")
  mapping$Gene <- GeneStructureTools::removeVersion(mapping$peak)
  mapping1 <- mapping[which(mapping$peak %in% dat$Var.2), ]
  
  aaa2 <- aaa1[which((aaa1$SYMBOL %in% dat$Gene) & aaa1$Strand == "+"), ]
  aaa2 <- aaa2[complete.cases(aaa2),]
  aaa2$uid2 <- paste(aaa2$seqnames, aaa2$start, aaa2$end, sep="_")
  aaa2 <- aaa2[which(aaa2$uid2 %in% mapping1$uid2), ]
  
  outdir <- paste0(c, "/upstream/")
  if(!dir.exists(outdir)) {
      dir.create(outdir, recursive = T)
  }
  
  write.table(x=aaa2[,c("uid", "seqnames", "start", "end","Strand")], file = paste0(outdir, "/atac_", c, "_DE_genes_upstream.homer"), quote = F, row.names = F, col.names = F, sep = '\t')
  
  homer_file <- paste0(outdir, "/atac_", c, "_DE_genes_upstream.homer")
  
  ## Find Motifs ##
  com <- paste0("perl ", homer_path_bin, "/findMotifsGenome.pl ", homer_file, " hg38 ", outdir)
  system(com)
  
}


# Annotation #
for(c in cells) {
    # Combine found motifs
    outdir <- paste0(c, "/upstream/")
    MotifOutputDirectory <- "motif_test/"
    homer_file <- paste0(outdir, "/atac_", c, "_DE_genes_upstream.homer")
    
    # Combine motifs in a single file
    com <- paste0("ls -tr ", outdir, "/homerResults ", " | grep motif[0-9]*..motif")
    out <- system(com, intern = TRUE)
    outdir1 <- paste0(outdir, "/homerResults/")
    motif_file <- paste0(outdir1, "/combined.motif")
    com <- paste("cat", paste(paste0(outdir1, out), collapse=" "), paste0("> ", motif_file))
    system(com)
    
    ann_base_dir <- paste0(outdir, "/annotated/")
    if(!dir.exists(ann_base_dir)) {
        dir.create(ann_base_dir)
    }
    
    # Annotate motifs
    com <- paste0("perl ", homer_path_bin, "/findMotifsGenome.pl ", homer_file, " hg38 ", MotifOutputDirectory, " -find ", motif_file, " > ", outdir, "/annotated/homerResults_motif_annotated.txt")
    system(com)
    
    res <- fread(paste0(outdir, "/annotated/homerResults_motif_annotated.txt"))
    res$motif_name2=do.call(rbind, strsplit(x = res$`Motif Name`, split = "BestGuess:"))[,2]
    anno <- read.xlsx(paste0(outdir, "homerResults.xlsx"))
    anno <- janitor::clean_names(anno)
    anno$motif_name2 <- do.call(rbind, strsplit(x = anno$best_match_details, split = "More Information | Similar Motifs Found"))[,1]
    anno <- merge(res, anno, by="motif_name2")
    anno$p_value <- as.numeric(anno$p_value)
    anno1 <- anno
    anno1 <- anno1[order(anno1$p_value), ]
    
    com <- paste0("perl ", homer_path_bin, "/annotatePeaks.pl ", homer_file, " hg38 ", "-strand +",  " > ", outdir, "/annotated/upstream_peaks_annotated.txt")
    system(com)
    anno2 <- fread(paste0(outdir, "/annotated/upstream_peaks_annotated.txt"))
    colnames(anno2)[1] <- "PositionID"
    anno2 <- merge(anno2, anno1, by="PositionID")
    
    write.xlsx(anno2, file=paste0(outdir, "/annotated/homerResults_upstream_peaks_annotated.xlsx"))
}

