---
title: "Individual cells + bulk (ridge regression)"
subtitle: "Variance filtering, top 1000 genes"
author: "Ilya Zhbanikov"
date: "`r Sys.Date()`"
output: 
    html_document: 
        toc: true
        toc_depth: 5
---

```{r setup, include=FALSE}

source("ml.R")
source("helper.R")
source("libraries.R")
source("model_group.R")
source("multiplot.R")
source("pyramid.plot.R")
source("utils.r")


library(ggfortify)
library(caret)
library( edgeR )
library( ggplot2 )
library( limma )
library(nnet)
library(gridExtra)   # to put more
library(grid) 
library(tidyverse)
library(dplyr)
library(ggVennDiagram)
library( tableone )
library(flextable)
library(stringi)
library(openxlsx)

#set.seed(123)

knitr::opts_chunk$set(echo = FALSE, message = FALSE, warning=FALSE)

alpha_reg <- 0 # 0 - Ridge, 1 - LASSO

#set.seed(123)
top <- 50
samples_excluded <- c("21092", "27764", "44758", "56904")
x_comm_names <- c("CD4_mature_T_cells", "CD4_naive_T_cells", "CD8_T_cells", "CD14_monocytes", "Dendritic_cells")

genes <- genes_all <- list()

set.seed(1234)
```

```{r functions}


cells <- c("B_cells", "CD4_mature_T_cells", "CD4_naive_T_cells", "CD8_T_cells", "CD14_monocytes", "CD16_monocytes", "Dendritic_cells", "Inflammatory_T_cells", "NK_cells")
cell_names <- c("B cells", "CD4+ memory T cells", "CD4+ naïve T cells", "CD8+ T cells", "CD14+ monocytes", "CD16+ monocytes", "Dendritic cells", "Inflammatory T cells", "NK cells")

key <- read.xlsx("qc.xlsx") # 


dem <- fread("dem.csv") # 
allkey <- read.xls("allkey.xlsx")
allkey <- allkey[which(allkey$assay == "scMultiome"), ]
allkey <- merge(allkey, dem, by = c("subject_id", "timepoint"))

```



## GEX

```{r, results='asis', fig.width=20, fig.height=10}
pp_boxplots <- list()

result_gex <- list()
result_gex_full <- list()

result_gex_notmm <- list()
result_gex_notmm_full <- list()

result_gex_notmmlog <- list()
result_gex_notmmlog_full <- list()

result_gex_notmmlogcpm <- list()
result_gex_notmmlogcpm_full <- list()

# Table1: AUC, cell type and # of samples
table1 <- data.frame(matrix(nrow=length(cells), ncol=6))
rownames(table1) <- cells
colnames(table1) <- c("Cell type", "auROC TMM+log", "auROC (no TMM)", "auROC\n(no TMM, no log)", "auROC\n(counts as is)", "N samples")

pp <- list()
pp_notmm <- list()
pp_notmmlog <- list()
pp_notmmlogcpm <- list()


for(i in 1:length(cells)) {
    c <- cells[i]
    key1 <- read.csv(paste0(c, "_key.csv"), header = T)
    allkey1 <- allkey[which(allkey$aliquot_id %in% key1$SampleID), ]
    
    # TMM
    data <- readRDS(paste0(c, "_gex.rds"))
    df <- as.data.frame(cbind(Exposure_cat=key1$CohortBinary, data))

    ans <- compute.fit.loocv_updated(df=df, outcome = "Exposure_cat", alpha = alpha_reg,
                                     covariates = colnames(df[, 2:ncol(df)]), direction = "auto")
    
    result_gex[[c]] <- ans$roc.testing
    result_gex_full[[c]] <- ans
    
    pp <- update_pp( df, ans, pp, prefix="GEX_", c )
    
    # Additional boxplots
    ddf <- pp[[paste0("GEX_", c)]]
    ddf <- merge(ddf, allkey1, by.x="Sample", by.y="aliquot_id")
    aaa <- ddf[,c(1:10, 78:82)]
    aaa$IndicatorDeath <- factor(ifelse(aaa$ordinal_8point_peak_score == "8- Death", 1, 
                                        ifelse(aaa$ordinal_8point_peak_score == "", NA, 0)))
    aaa$PP <- as.numeric(aaa$PP)
    pp_boxplot <- ggstatsplot::ggbetweenstats(x=IndicatorDeath, y=PP, data=aaa[which(aaa$`Case/control` == 1), ], title=c, results.subtitle=T, type = "nonparametric")
    pp_boxplots[[c]] <- pp_boxplot
    
    # No TMM
    data <- readRDS(paste0(c, "_notmm_gex.rds"))
    df <- as.data.frame(cbind(Exposure_cat=key1$CohortBinary, data))

    ans <- compute.fit.loocv_updated(df=df, outcome = "Exposure_cat", alpha = alpha_reg,
                                     covariates = colnames(df[, 2:ncol(df)]), direction = "auto")
    
    result_gex_notmm[[c]] <- ans$roc.testing
    result_gex_notmm_full[[c]] <- ans
    
    pp_notmm <- update_pp( df, ans, pp_notmm, prefix="GEX_", c )
    
    
    # No TMM, log
    data <- readRDS(paste0(c, "_notmmlog_gex.rds"))
    df <- as.data.frame(cbind(Exposure_cat=key1$CohortBinary, data))

    ans <- compute.fit.loocv_updated(df=df, outcome = "Exposure_cat", alpha = alpha_reg,
                                     covariates = colnames(df[, 2:ncol(df)]), direction = "auto")
    
    result_gex_notmmlog[[c]] <- ans$roc.testing
    result_gex_notmmlog_full[[c]] <- ans
    
    pp_notmmlog <- update_pp( df, ans, pp_notmmlog, prefix="GEX_", c )
    
    
    # As is
    data <- readRDS(paste0(c, "_notmmlogcpm_gex.rds"))
    df <- as.data.frame(cbind(Exposure_cat=key1$CohortBinary, data))

    ans <- compute.fit.loocv_updated(df=df, outcome = "Exposure_cat", alpha = alpha_reg,
                                     covariates = colnames(df[, 2:ncol(df)]), direction = "auto")
    
    result_gex_notmmlogcpm[[c]] <- ans$roc.testing
    result_gex_notmmlogcpm_full[[c]] <- ans
    
    pp_notmmlogcpm <- update_pp( df, ans, pp_notmmlogcpm, prefix="GEX_", c )
    
    #------------------------------#
    
    
    table1[which(rownames(table1) == c), "Cell type"] <- c
    table1[which(rownames(table1) == c), "auROC TMM+log"] <- round(result_gex[[c]]$auc, 2)
    table1[which(rownames(table1) == c), "auROC (no TMM)"] <- round(result_gex_notmm[[c]]$auc, 2)
    table1[which(rownames(table1) == c), "auROC\n(no TMM, no log)"] <- round(result_gex_notmmlog[[c]]$auc, 2)
    table1[which(rownames(table1) == c), "auROC\n(counts as is)"] <- round(result_gex_notmmlogcpm[[c]]$auc, 2)
    table1[which(rownames(table1) == c), "N samples"] <- nrow(data)
    
    
    

}


## TMM
#--------------------------------------------------------------------------------------------------------#
p1 <- printAUC(result_gex, "auROCs by cell type (scRNA)", legend_labels = cell_names)

## No TMM
#--------------------------------------------------------------------------------------------------------#
p2 <- printAUC(result_gex_notmm, "auROCs by cell type, no TMM", legend_labels = cell_names)

## No TMM, no log
#--------------------------------------------------------------------------------------------------------#
p3 <- printAUC(result_gex_notmmlog, "auROCs by cell type, no TMM, no log (only CPM)", legend_labels = cell_names)

## No TMM, log, cpm (counts as is)
#--------------------------------------------------------------------------------------------------------#
p4 <- printAUC(result_gex_notmmlogcpm, "auROCs by cell type, counts 'as is': no TMM, no log, no CPM", legend_labels = cell_names)


multiplot(p1, p3, cols=2, byrow = TRUE)


setEPS()
    postscript("Fig2A.eps", width = 10, height = 10)
    p1
dev.off()


```


## ATAC

```{r, eval=T, fig.width=20, fig.height=10}
pp_boxplots <- list()
result <- list()
result_full <- list()

result_notmm <- list()
result_notmm_full <- list()

result_notmmlog <- list()
result_notmmlog_full <- list()

result_notmmlogcpm <- list()
result_notmmlogcpm_full <- list()

# Table1: AUC, cell type and # of samples
table1 <- data.frame(matrix(nrow=length(cells), ncol=6))
rownames(table1) <- cells
colnames(table1) <- c("Cell type", "auROC TMM+log", "auROC (no TMM)", "auROC\n(no TMM, no log)", "auROC\n(counts as is)", "N samples")
#pp <- list()
for(i in 1:length(cells)) {
    c <- cells[i]
    key1 <- read.csv(paste0(c, "_key.csv"), header = T)
    allkey1 <- allkey[which(allkey$aliquot_id %in% key1$SampleID), ]
    
    # TMM
    data <- readRDS(paste0(c, "_atac.rds"))
    df <- as.data.frame(cbind(Exposure_cat=key1$CohortBinary, data))

    ans <- compute.fit.loocv_updated(df=df, outcome = "Exposure_cat", alpha = alpha_reg,
                                     covariates = colnames(df[, 2:ncol(df)]), direction = "auto")
    
    result[[c]] <- ans$roc.testing
    result_full[[c]] <- ans
    
    
    #dat <- as.data.frame(cbind(rownames(df), ans$roc.testing$original.response, round(ans$predicted, 3)))
    #colnames(dat) <- c("Sample", "Case/control", "PP")
    #pp[[paste0("ATAC_", c)]] <- dat
    
    pp <- update_pp( df, ans, pp, prefix="ATAC_", c )
    
    # Additional boxplots
    ddf <- pp[[paste0("ATAC_", c)]]
    ddf <- merge(ddf, allkey1, by.x="Sample", by.y="aliquot_id")
    aaa <- ddf[,c(1:10, 78:82)]
    aaa$IndicatorDeath <- factor(ifelse(aaa$ordinal_8point_peak_score == "8- Death", 1, 
                                        ifelse(aaa$ordinal_8point_peak_score == "", NA, 0)))
    aaa$PP <- as.numeric(aaa$PP)
    pp_boxplot <- ggstatsplot::ggbetweenstats(x=IndicatorDeath, y=PP, data=aaa[which(aaa$`Case/control` == 1), ], title=c, results.subtitle=T, type = "nonparametric")
    pp_boxplots[[c]] <- pp_boxplot
    
    
    # No TMM
    data <- readRDS(paste0(c, "_notmm_atac.rds"))
    df <- as.data.frame(cbind(Exposure_cat=key1$CohortBinary, data))

    ans <- compute.fit.loocv_updated(df=df, outcome = "Exposure_cat", alpha = alpha_reg,
                                     covariates = colnames(df[, 2:ncol(df)]), direction = "auto")
    
    result_notmm[[c]] <- ans$roc.testing
    result_notmm_full[[c]] <- ans
    
    pp_notmm <- update_pp( df, ans, pp_notmm, prefix="ATAC_", c )
    
    
    
    # No TMM, log
    data <- readRDS(paste0(c, "_notmmlog_atac.rds"))
    df <- as.data.frame(cbind(Exposure_cat=key1$CohortBinary, data))

    ans <- compute.fit.loocv_updated(df=df, outcome = "Exposure_cat", alpha = alpha_reg,
                                     covariates = colnames(df[, 2:ncol(df)]), direction = "auto")
    
    result_notmmlog[[c]] <- ans$roc.testing
    result_notmmlog_full[[c]] <- ans
    
    pp_notmmlog <- update_pp( df, ans, pp_notmmlog, prefix="ATAC_", c )
    
    # As is
    data <- readRDS(paste0(c, "_notmmlogcpm_atac.rds"))
    df <- as.data.frame(cbind(Exposure_cat=key1$CohortBinary, data))

    ans <- compute.fit.loocv_updated(df=df, outcome = "Exposure_cat", alpha = alpha_reg,
                                     covariates = colnames(df[, 2:ncol(df)]), direction = "auto")
    
    result_notmmlogcpm[[c]] <- ans$roc.testing
    result_notmmlogcpm_full[[c]] <- ans
    
    pp_notmmlogcpm <- update_pp( df, ans, pp_notmmlogcpm, prefix="ATAC_", c )
    
    #------------------------------#
    
    
    table1[which(rownames(table1) == c), "Cell type"] <- c
    table1[which(rownames(table1) == c), "auROC TMM+log"] <- round(result[[c]]$auc, 2)
    table1[which(rownames(table1) == c), "auROC (no TMM)"] <- round(result_notmm[[c]]$auc, 2)
    table1[which(rownames(table1) == c), "auROC\n(no TMM, no log)"] <- round(result_notmmlog[[c]]$auc, 2)
    table1[which(rownames(table1) == c), "auROC\n(counts as is)"] <- round(result_notmmlogcpm[[c]]$auc, 2)
    table1[which(rownames(table1) == c), "N samples"] <- nrow(data)
    

}

## TMM
#--------------------------------------------------------------------------------------------------------#
p1 <- printAUC(result, "auROCs by cell type (scATAC)", legend_labels = cell_names)

## No TMM
#--------------------------------------------------------------------------------------------------------#
p2 <- printAUC(result_notmm, "auROCs by cell type, no TMM", legend_labels = cell_names)

## No TMM, log
#--------------------------------------------------------------------------------------------------------#
p3 <- printAUC(result_notmmlog, "auROCs by cell type, no TMM, no log (only CPM)", legend_labels = cell_names)

## No TMM, log, cpm (counts as is)
#--------------------------------------------------------------------------------------------------------#
p4 <- printAUC(result_notmmlogcpm, "auROCs by cell type, counts 'as is': no TMM, no log, no CPM", legend_labels = cell_names)


multiplot(p1, p3, cols=2, byrow = TRUE)


setEPS()
    postscript("Fig2C.eps", width = 10, height = 10)
p1
dev.off()


```


## Pseudo-bulk data

* Weighted sum after log-transform and TMM.
* Weight: (1/(Ncells/Total cells)) / sum( (1/(Ncells/Total cells) )
* Weights sum to 1.


### GEX pseudo-bulk

```{r, fig.width=20, fig.height=10}
pp_boxplots <- list()
result <- list()
result_full <- list()
result_notmm <- list()
result_notmmlog <- list()
result_notmmlogcpm <- list()
#pp <- list()

key <- read.csv(paste0("bulk_key_average.csv"), header = T)
allkey1 <- allkey[which(allkey$aliquot_id %in% key$SampleID), ]
    
# TMM
data <- readRDS(paste0("bulk_gex_average.rds"))
df <- as.data.frame(cbind(Exposure_cat=key$CohortBinary, data))

ans <- compute.fit.loocv_updated(df=df, outcome = "Exposure_cat", alpha = alpha_reg,
                                     covariates = colnames(df[, 2:ncol(df)]), direction = "auto" )
    
result[["Bulk GEX"]] <- ans$roc.testing
results_gex <- result[["Bulk GEX"]]
result_full[["Bulk GEX"]] <- ans

pp <- update_pp( df, ans, pp, prefix="GEX_pseudobulk", "" )

# Additional boxplots
    ddf <- pp[["GEX_pseudobulk"]]
    ddf <- merge(ddf, allkey1, by.x="Sample", by.y="aliquot_id")
    aaa <- ddf[,c(1:10, 78:82)]
    aaa$IndicatorDeath <- factor(ifelse(aaa$ordinal_8point_peak_score == "8- Death", 1, 
                                        ifelse(aaa$ordinal_8point_peak_score == "", NA, 0)))
    aaa$PP <- as.numeric(aaa$PP)
    pp_boxplot <- ggstatsplot::ggbetweenstats(x=IndicatorDeath, y=PP, data=aaa[which(aaa$`Case/control` == 1), ], title="GEX, pseudobulk", results.subtitle=T, type = "nonparametric")

# No TMM, log
data <- readRDS(paste0("bulk_gex_average_asis.rds"))
df <- as.data.frame(cbind(Exposure_cat=key$CohortBinary, data))

ans <- compute.fit.loocv_updated(df=df, outcome = "Exposure_cat", alpha = alpha_reg,
                                     covariates = colnames(df[, 2:ncol(df)]), direction = "auto" )
    
result_notmmlog[["Bulk GEX"]] <- ans$roc.testing
result_notmmlog_full[["Bulk GEX"]] <- ans



## TMM
#--------------------------------------------------------------------------------------------------------#
p1 <- printAUC(result, "auROC, CPM TMM+log (scRNA)")

p2 <- printAUC(result_notmmlog, "auROC, CPM, No TMM, no log")

multiplot(p1, p2, cols = 2)


setEPS()
postscript("GEX_Pseudobulk_AUC_v8.eps", width = 10, height = 10)
p1
dev.off()
```


### ATAC pseudo-bulk

```{r, fig.width=10, fig.height=7}
pp_boxplots <- list()
result <- list()
result_full <- list()
result_notmm <- list()
result_notmmlog <- list()
result_notmmlogcpm <- list()
#pp <- list()

key <- read.csv(paste0("bulk_key_average.csv"), header = T)
allkey1 <- allkey[which(allkey$aliquot_id %in% key$SampleID), ]

# TMM
data <- readRDS(paste0("bulk_atac_average.rds"))
covariates <- colnames(data)

df <- data.frame(data)
df$Exposure_cat <- key$CohortBinary

ans <- compute.fit.loocv_updated(df=df, outcome = "Exposure_cat", alpha = alpha_reg, direction="auto", covariates = covariates)
    
result[["Bulk ATAC"]] <- ans$roc.testing
results_atac <- result[["Bulk ATAC"]]
result_full[["Bulk ATAC"]] <- ans

pp <- update_pp( df, ans, pp, prefix="ATAC_pseudobulk", "" )
    
# Additional boxplots
    ddf <- pp[["ATAC_pseudobulk"]]
    ddf <- merge(ddf, allkey1, by.x="Sample", by.y="aliquot_id")
    aaa <- ddf[,c(1:10, 78:82)]
    aaa$IndicatorDeath <- factor(ifelse(aaa$ordinal_8point_peak_score == "8- Death", 1, 
                                        ifelse(aaa$ordinal_8point_peak_score == "", NA, 0)))
    aaa$PP <- as.numeric(aaa$PP)
    pp_boxplot <- ggstatsplot::ggbetweenstats(x=IndicatorDeath, y=PP, data=aaa[which(aaa$`Case/control` == 1), ], title="ATAC pseudobulk", results.subtitle=T, type = "nonparametric")
    

# No TMM
data <- readRDS(paste0("bulk_notmm_atac.rds"))
df <- as.data.frame(cbind(Exposure_cat=key$CohortBinary, data))

ans <- compute.fit.loocv_updated(df=df, outcome = "Exposure_cat", alpha = alpha_reg, direction="auto",
                                     covariates = colnames(df[, 2:ncol(df)]))
    
result_notmm[["Bulk ATAC"]] <- ans$roc.testing

pp_notmm <- update_pp( df, ans, pp_notmm, prefix="ATAC_pseudobulk", "" )


#-----------------------------------------------------------------------------------#
# No TMM, log
data <- readRDS(paste0("bulk_atac_average_asis.rds"))
covariates <- colnames(data)
df <- data.frame(data)
df$Exposure_cat <- key$CohortBinary

ans <- compute.fit.loocv_updated(df=df, outcome = "Exposure_cat", alpha = alpha_reg, direction="auto", covariates = covariates)
    
result_notmmlog[["Bulk ATAC"]] <- ans$roc.testing

pp_notmmlog <- update_pp( df, ans, pp_notmmlog, prefix="ATAC_pseudobulk", "" )


#-----------------------------------------------------------------------------------#


#-----------------------------------------------------------------------------------#
# No TMM, log, cpm
data <- readRDS(paste0("bulk_atac_average_asis_2.rds"))
covariates <- colnames(data)
df <- data.frame(data)
df$Exposure_cat <- key$CohortBinary


ans <- compute.fit.loocv_updated(df=df, outcome = "Exposure_cat", alpha = alpha_reg, direction="auto", covariates = covariates)
    
result_notmmlogcpm[["Bulk ATAC"]] <- ans$roc.testing

pp_notmmlogcpm <- update_pp( df, ans, pp_notmmlogcpm, prefix="ATAC_pseudobulk", "" )


#-----------------------------------------------------------------------------------#


## TMM
#--------------------------------------------------------------------------------------------------------#
p1 <- printAUC(result, "auROC, CPM TMM+log (scATAC)")

## No TMM
#--------------------------------------------------------------------------------------------------------#
p2 <- printAUC(result_notmm, "auROC, no TMM")

## No TMM, log
#--------------------------------------------------------------------------------------------------------#
p3 <- printAUC(result_notmmlog, "auROC, CPM, no TMM, no log")

## No TMM, log, cpm (counts as is)
#--------------------------------------------------------------------------------------------------------#
p4 <- printAUC(result_notmmlogcpm, "auROC, counts 'as is' (no CPM, log and TMM)")


#multiplot(p1, p2, p3, p4, cols=2, byrow = TRUE)

multiplot(p1, p3, cols=2, byrow = TRUE)


setEPS()
postscript("ATAC_Pseudobulk_AUC_v8.eps", width = 10, height = 10)
p1
dev.off()
```


