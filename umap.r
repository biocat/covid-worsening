
library(M3C)
library(ggplot2)
library(openxlsx)
library(Seurat)
library(Signac)
library(SeuratData)
library( edgeR )
library(TxDb.Hsapiens.UCSC.hg19.knownGene)
library(org.Hs.eg.db)
library(ChIPseeker)


normalize <- function(data) {
  
  xpr_raw <- DGEList( counts = data )
  cpm <- cpm( xpr_raw )
  
  ix <- rowMeans( cpm > 1 ) >= 0.5
  xpr <- xpr_raw[ix,]
  cat( sprintf( "transcripts :: all: %d, after filtering: %d\n", dim( xpr_raw )[1], dim( xpr )[1] ) )
  
  xpr_norm <- calcNormFactors(xpr, method = "TMM")
  
  return(list(xpr_norm= xpr_norm, xpr = xpr))
  
}

removeTechnicalStuff <- function( data, genes_to_exclude=c("MIR", "MALAT", "RPL", "MT", "RPS") ) {
  deleted <- c()
  for(c in genes_to_exclude) {
    deleted <- c(deleted, rownames(data)[grep(c, rownames(data), ignore.case = TRUE)])
  }
  
  res <- data[which(!(rownames(data) %in% deleted)),]
  
  return(res)
}




# We want 3 figures, one colored by cell type, one colored by group (worsening vs. control), and one colored by samples that have cell type label and those who don't.

cells <- c("CD4_mature_T_cells", "CD4_naive_T_cells", "CD8_T_cells", "CD14_monocytes", "CD16_monocytes", "Dendritic_cells", "Inflammatory_T_cells", "NK_cells")

data1 <- as.data.frame(readRDS("B_cells_gex.rds"))
colnames(data1) <- paste0(colnames(data1), "_", "B_cells")
lbs <- replicate( n=ncol(data1), "B_cells" )
data1$sample <- rownames(data1)


#for(i in 1:length(cells)) {
for(i in 1:length(cells)) {
  c <- cells[i]
  d <- as.data.frame(readRDS(paste0(c, "_gex.rds")))
  colnames(d) <- paste0(colnames(d), "_", c)
  lbs <- c( lbs, replicate( n=ncol(d), c ) )
  d$sample <- rownames(d)
  
  data1 <- merge(data1, d, by="sample", all=T)

}




# By cell type
umap(as.data.frame(data1[complete.cases(data1),-1]), 
     labels=as.factor(lbs),
     controlscale=T, scale = 3, 
     legendtitle = "Cell types")



# By cohort and samples (bulk data)

key <- openxlsx::read.xlsx("key.xlsx")
## GEX
data <- as.data.frame(readRDS("GEX_pseudobulk_matrices.rds"))
key1 <- key[ which(key$SampleID %in% rownames(data)), ]
data <- data[ match(key1$SampleID, rownames(data)), ]
all( key1$SampleID == rownames(data) )
lbs <- key1$Cohort

clean <- normalize(t(data))
clean <- removeTechnicalStuff(clean$xpr_norm)

# By cohort
umap(as.data.frame(clean$counts), 
     labels=as.factor(lbs),
     controlscale=T, scale = 3, 
     legendtitle = "Cohort")

# By samples
key1$HasCellLabel <- ifelse( key1$SampleID %in% data1$sample, "Yes", "No" ) 
lbs <- key1$HasCellLabel
umap(as.data.frame(clean$counts), 
     labels=as.factor(lbs),
     controlscale=T, scale = 3, 
     legendtitle = "Has cell label")

ump <- uwot::umap(as.data.frame(t(clean$counts)), pca = 25, init = "spca")
embed_img(as.data.frame(t(clean$counts)), ump, pc_axes = TRUE, equal_axes = TRUE, alpha_scale = 0.5, title = "UMAP", cex = 1)




# By cell type, attempt 2

# GEX
load("GEX_gene_by_cell_matrix.RData")
load("GEX_cell_type_labels.RData")
data <- counts

# Map cell barcodes to cell labels
counts <- counts[, match( GEX_cell_type_labels$GEX_barcode, colnames(counts) )]
colnames(counts) <- GEX_cell_type_labels$celltype

ump <- uwot::umap(as.data.frame(t(counts)), pca = 100)

# Combine with original data and blended colours
df <- cbind.data.frame(
  setNames(as.data.frame(ump), c("x", "y")), 
  celltype=colnames(counts)
  )

ggplot(df, aes(x, y, colour = celltype)) + 
  geom_point() +
  ggtitle("UMAP")


# ATAC
load("ATAC_peak_by_cell_matrix.RData")
load("ATAC_cell_type_labels.RData")
data <- ATAC_peak_by_cell_matrix

# Map cell barcodes to cell labels
data <- data[, match( ATAC_cell_type_labels$ATAC_barcode, colnames(data) )]
colnames(data) <- ATAC_cell_type_labels$celltype

ump <- uwot::umap(as.data.frame(t(data)), pca = 100)

# Combine with original data and blended colours
df <- cbind.data.frame(
  setNames(as.data.frame(ump), c("x", "y")), 
  celltype=colnames(data)
)

ggplot(df, aes(x, y, colour = celltype)) + 
  geom_point() +
  ggtitle("UMAP ATAC")




